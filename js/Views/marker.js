RM.Marker = class {
    constructor(location, textBox, properties) {
        this.location = location;
        this.widget   = RM.createMarker(location, textBox, properties);
    }

    // region Public members
    isDisplayed() {
        return this.widget._map !== null;
    }
    // endregion

    // region Static members
    static createStartMarker(trackInformations) {
        let latlng  = trackInformations.startLocation;
        let textBox = RM.startMarkerBox.formatUnicorn(trackInformations).toHtmlElement();

        return new RM.Marker(latlng, textBox);
    }
    // endregion
};