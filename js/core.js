// region Initialization
$(function () {
    RMLanguages.initializeLanguage();
});

$(document).on(RM.Events.languageInitialized, function () {
    RM.map = L.map('mapId')
        .setView(RM.defaultLocation, RM.defaultZoomLevel)
        .on('moveend', function () {
            RM.showMarkersWihtinXKm($('#maxDistance').val());
        });

    const riderAgreement = RM.RiderLocationAgreement();
    if (riderAgreement === 'true') {
        RM.centerMapOnRiderLocation();
    } else if (riderAgreement === null) {
        RM.askForRiderLocation();
    }

    L.tileLayer(RM.selectedMapType[0], RM.selectedMapType[1]).addTo(RM.map);

    initializeGpxTestFiles(); // TODO : erase that line
});

// endregion

// region Map
RM.centerMapOnRiderLocation = function () {
    if (RM.riderLocation !== undefined && RM.riderLocation !== null) {
        RM.centerMapOn(RM.riderLocation);
    }
    else if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            RM.riderLocation = [
                position.coords.latitude,
                position.coords.longitude
            ];
            RM.displayMarker(RM.createMarker(RM.riderLocation, RM.l.yourLocation, {
                icon: L.icon({
                    iconUrl: RM.Icons.RiderLocation
                })
            }));
            RM.centerMapOn(RM.riderLocation);
        });
    }
};

RM.centerMapOnTrack = function (idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);
    RM.map.fitBounds(track.widget.getBounds());
};

RM.centerMapOn = function (latlng) {
    RM.map.setView(latlng, RM.map.zoom);
};

RM.clearMap = function() {
    for (const [ignored,track] of iterate_object(RM.tracks)) {
        if (track.widget !== undefined && track.widget !== null) {
            track.widget.removeFrom(RM.map);
        }
        if (track.startWidget !== undefined && track.startWidget !== null) {
            track.startWidget.remove();
        }
    }
};
// endregion

// region Tracks
RM.getTrackIdFromTrackNumber = function(trackNumber) {
    return RM.trackIdSuffix + trackNumber;
};

RM.getTrack = function(idOrNumberOrTrack) {
    let trackId = idOrNumberOrTrack;
    let track = idOrNumberOrTrack;
    if (typeof idOrNumberOrTrack === "number") {
        trackId = RM.getTrackIdFromTrackNumber(idOrNumberOrTrack);
        track = RM.tracks[trackId];
    } else if (typeof idOrNumberOrTrack === "string") {
        track = RM.tracks[trackId];
    }
    return track;
};

RM.isTrackDisplayed = function(idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);
    return track.widget !== null && track.widget._map !== undefined && track.widget._map !== null;
};

RM.loadGpx = function (url, onSuccessCallback) {
    let xhr = window.XMLHttpRequest ?
        new XMLHttpRequest() :
        new ActiveXObject('Microsoft.XMLHTTP');

    if (xhr.overrideMimeType) {
        xhr.overrideMimeType('text/xml');
    }

    xhr.open('GET', url, true);
    xhr.onload = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                onSuccessCallback(xhr.responseXML);
            } else {
                console.error(xhr.statusText);
            }
        }
    };
    xhr.send();
};

RM.toggleTrackDisplay = function (idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);
    if (track.widget === null) {
        RM.computeTrack(track);
    }

    const addOrRemove = RM.isTrackDisplayed(track) ? 'removeFrom' : 'addTo';
    track.widget[addOrRemove](RM.map);
    track.startWidget._popup.remove();

    if (RM.isTrackDisplayed(track)) {
        RM.centerMapOnTrack(track);
    }
};

RM.goToTrack = function (idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);

    if (!RM.isTrackDisplayed(track)) {
        RM.computeTrack(track);
        RM.toggleTrackDisplay(track);
    }
    RM.centerMapOnTrack(track);
};

RM.computeTrack = function (idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);
    if (track.widget !== null) return track.widget;

    const trackDrawing = new L.GPX(track.string,
        {
            async: false,
            polyline_options: RM.RideTypes.Cross.style,
            marker_options: {
                startIconUrl: false,
                endIconUrl: false
            }
        }
    );
    track.widget = trackDrawing;

    return trackDrawing;
};

RM.getStartCoordinates = function (gpx) {
    let startPoint = gpx.getElementsByTagName('trkpt')[0];
    if (startPoint === undefined) {
        startPoint = gpx.getElementsByTagName('rtept')[0];
        if (startPoint === undefined) {
            return null;
        }
    }

    const lat = Number(startPoint.getAttribute('lat'));
    const lng = Number(startPoint.getAttribute('lon'));
    return [lat, lng];
};

RM.addTrackToList = function (idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);
    let container = $('#trackList');
    let textBox = $(RM.trackBoxListItem.formatUnicorn(track.informations).toHtmlElement());
    container.append(textBox);
};

RM.showTracksWithinXKM = function(x) {
    //RM.clearMap();
    for (const [ignored,track] of iterate_object(RM.tracks)) {
        const distance = RM.map.distance(RM.map.getCenter(), track.informations.startPosition);
        if (x === undefined  || x === null || distance <= x*1000) {
            track.startWidget.addTo(RM.map);
            RM.computeTrack(track).addTo(RM.map);
        } else {
            track.startWidget.remove();
            if (track.widget !== null) {
                track.widget.remove();
            }
        }
    }
};

RM.calculateLengthOfTrack = function(idOrNumberOrTrack) {
    const track = RM.getTrack(idOrNumberOrTrack);

    let length = 0;
    let segments = $(track.xml).find('trkpt');
    segments.each(function(index, current) {
        if (index === 0) return true;
        const currentElement = $(current);
        const currentLatlng = [currentElement.attr('lat'), currentElement.attr('lon')];

        const lastElement = $(segments[index-1]);
        const lastLatlng = [lastElement.attr('lat'), lastElement.attr('lon')];

        length += RM.map.distance(currentLatlng, lastLatlng);
    });

    return length;
};
// endregion

// region Markers
RM.displayMarker = function (marker) {
    marker.addTo(RM.map);
    return marker;
};

RM.createMarker = function(latlng, text, properties) {
    let marker = L.marker(latlng, properties);
    marker.bindPopup(text);
    return marker;
};

RM.createStartMarker = function (informations) {
    let latlng = informations.startPosition;
    let textBox = RM.startMarkerBox.formatUnicorn(informations).toHtmlElement();
    return RM.displayMarker(RM.createMarker(latlng, textBox));
};

RM.showMarkersWihtinXKm = function (x) {
    //RM.clearMap();
    for (const [ignored,track] of iterate_object(RM.tracks)) {
        const distance = RM.map.distance(RM.map.getCenter(), track.informations.startPosition);
        if (x === undefined  || x === null || distance <= x*1000) {
            track.startWidget.addTo(RM.map);
        } else {
            track.startWidget.remove();
            if (track.widget !== null) {
                track.widget.remove();
            }
        }
    }
};
// endregion

// region Rider's location
RM.askForRiderLocation = function() {
    if (RM.RiderLocationAgreement() === true) {
        RM.centerMapOnRiderLocation();
        return;
    }
    const actions = [
        [RM.l.accept, RM.acceptRiderLocation],
        [RM.l.decline, RM.declineRiderLocation]
    ];
    Templates.Modals.displayModal(Templates.Modals.createModal(RM.l.accept, RM.l.authorizeRiderLocation, 'locationModal', actions));
};

RM.acceptRiderLocation = function() {
    RM.RiderLocationAgreement(true);
    RM.centerMapOnRiderLocation();
};

RM.declineRiderLocation = function() {
    RM.RiderLocationAgreement(false);
};
// endregion

// region TODO : erase these functions

function initializeGpxTestFiles() {
    RM.GpxFiles.forEach(function (name) {
        RM.loadGpx(`gpx/${name}.gpx`, function (gpx) {
            let trackId = +new Date();
            let informations = {name: name, id: trackId, startPosition: RM.getStartCoordinates(gpx)};
            let startMarker = RM.createStartMarker(informations);
            const track = {
                xml: gpx,
                string: (new XMLSerializer()).serializeToString(gpx),
                widget: null,
                informations: informations,
                startWidget: startMarker
            };
            informations.length = Math.round(RM.calculateLengthOfTrack(track)/100) / 10;
            RM.tracks[`track${trackId}`] = track;
            RM.addTrackToList(track);
        });
    });
}

// endregion