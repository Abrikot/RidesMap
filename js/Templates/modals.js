Templates.Modals = {};

Templates.Modals.template =
    '<div class="modal fade show" role="dialog" id="{id}">' +
        '<div class="modal-dialog">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<h4>{title}</h4>' +
                    '<button type="button" data-dismiss="modal" class="close">×</button>' +
                '</div>' +
                '<div class="modal-body">{text}</div>' +
                '<div class="modal-footer">' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';

Templates.Modals.actionTemplate = '<button type="button" data-dismiss="modal" class="btn btn-default">{text}</button>';

Templates.Modals.createModal = function(title, text, id, actions) {
    let html = $(Templates.Modals.template.formatUnicorn({title: title, text: text, id: id}).toHtmlElement());
    let footer = html.find('.modal-footer');
    if (actions !== undefined && actions !== null) {
        for (const action of actions) {
            let htmlAction = $(Templates.Modals.actionTemplate.formatUnicorn({text: action[0]}).toHtmlElement()).click(action[1]);
            footer.append(htmlAction);
        }
    }
    return html;
};

Templates.Modals.createModalWithSelectList = function(title, text, id, elements, actions) {
    let html = $(Templates.Modals.template.formatUnicorn({title: title, text: text, id: id}).toHtmlElement());

    let body = html.find('.modal-body');
    let {selectBlock, selectList} = Templates.createSelectList(id, elements);
    body.append(selectBlock);

    let footer = html.find('.modal-footer');
    if (actions !== undefined && actions !== null) {
        for (const action of actions) {
            let htmlAction = $(Templates.Modals.actionTemplate.formatUnicorn({text: action[0]}).toHtmlElement()).click(function() {
                action[1](selectList.val());
            });
            footer.append(htmlAction);
        }
    }
    return html;
};

Templates.Modals.displayModal = function(modal) {
    $(document.body).append(modal);
    modal.modal('show');
};
