RM.TrackInformations = class {
    /**
     * Contains informations on a track
     * @param id                track's id
     * @param url               url of the .GPX file associated to this track
     * @param name              name of the track
     * @param startLocation     location of the start of this track [latitude, longitude]
     * @param type              type of this track
     * @param length            length of this track
     * @param heightDifference  height difference of this track
     */
    constructor(id, url, name, startLocation, type, length, heightDifference) {
        this.id               = id              ;
        this.url              = url             ;
        this.name             = name            ;
        this.startLocation    = startLocation   ;
        this.type             = type            ;
        this.length           = length          ;
        this.heightDifference = heightDifference;
    }
};