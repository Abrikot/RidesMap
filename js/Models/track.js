RM.Track = class {
    constructor(trackInformations) {
        this.trackInformations  = trackInformations ;

        // will be loaded later
        this.xmlVersion         = undefined         ;
        this.stringifiedVersion = undefined         ;
    }

    load(onSuccessCallback) {
        let xhr = window.XMLHttpRequest ?
            new XMLHttpRequest() :
            new ActiveXObject('Microsoft.XMLHTTP');

        if (xhr.overrideMimeType) {
            xhr.overrideMimeType('text/xml');
        }

        xhr.open('GET', this.trackInformations.url, true);
        xhr.onload = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    this.xmlVersion = xhr.responseXML;
                    onSuccessCallback(xhr.responseXML);
                } else {
                    console.error(xhr.statusText);
                }
            }
        };
        xhr.send();
    }

    get isLoaded (){
        return this.trackInformations !== undefined && this.trackInformations !== null;
    }
};