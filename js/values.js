// region Enums
RM.MapTypes = {
    France: [
        'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
        {
            maxZoom: 20,
            attribution: '&copy; Openstreetmap France | &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }
    ],
    ToopMap: [
        'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
        {
            maxZoom: 17,
            attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
        }
    ]
};
RM.RideTypes = {
    Road: {
        style: {
            color: 'green',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    },
    Cross: {
        style: {
            color: 'green',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    },
    Muni: {
        style: {
            color: 'green',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
        }
    }
};
RM.Icons = {
    BlackLocation: 'img/black-marker-icon.png',
    BlueLocation : 'img/blue-marker-icon.png'
};
RM.Icons.RiderLocation = RM.Icons.BlackLocation;
RM.Events = {
    languageInitialized: 'languageInitialized'
};
// endregion

// region Default values
RM.defaultLocation = [47.070122, 2.636719];
RM.defaultZoomLevel = 7;
// endregion

// region Constants
RM.startMarkerBox = "<div>{name}<br><a id='{id}' href='#' onclick='RM.toggleTrackDisplay({id})'>Toggle track display</a> </div>";
RM.trackBox = '<div> <a href="#" onclick="RM.goToTrack({id})">{name}</a></div>';
RM.descriptionBoxTemplate =
    '<div class="descriptionBox">' +
        '<h4>' +
            '<a href="#" onclick="RM.goToTrack({id})">' +
                '{name}' +
            '</a>' +
        '</h4>' +
        '<p>{length} km</p>' +
    '</div>';
RM.trackBoxListItem = `<li>${RM.descriptionBoxTemplate}</li>`;
RM.trackIdSuffix = 'track';
// endregion

// region Global variables
RM.map = null;
RM.selectedMapType = RM.MapTypes.ToopMap;
RM.riderLocation = null;
RM.tracks = {};
RM.l = RMLanguages.defaultLanguage;
// endregion

// region Local storage access

// Later functions are double accessors:
// no-arg-call is a getter where arg-call is a setter.
RM.RiderLocationAgreement = function(agreement) {
    const key = 'RM.RiderLocationAgreement';
    return RM.accessStorage(key, agreement, {getterMutator: agreement => agreement || agreement === "true"})
};

RM.Language = function(language) {
    const key = 'RM.Language.name';
    return RM.accessStorage(key, language,
        {
            setterMutator: language => language.englishLanguageName,
            getterMutator: language => RMLanguages.Dictionnaries[language]
        });
};

RM.accessStorage = function(key, value, {setterMutator, getterMutator}) {
    setterMutator = setterMutator || (valueToStore => valueToStore);
    getterMutator = getterMutator || (storedValue => storedValue);
    if (value === undefined) return getterMutator(localStorage.getItem(key));
    else localStorage.setItem(key, setterMutator(value));
};
// endregion

// region Stub TODO : erase these values
RM.GpxFiles = ['Poncin', 'Douvres'];
function getFirstTrack() {
    return RM.tracks[Object.keys(RM.tracks)[0]];
}
function getNthTrack(n) {
    return RM.tracks[Object.keys(RM.tracks)[n]];
}
// endregion